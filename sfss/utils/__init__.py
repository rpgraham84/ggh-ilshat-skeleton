# -*- coding: utf-8 -*-
import re
import os
import json
import traceback
import fractions

from collections import defaultdict, Counter
from itertools import izip_longest
from difflib import SequenceMatcher

from .categorizer import Categorizer

# General helper functions and data (mainly because DRY)
# ---------------------------------------------------------

numbers = {
    'single': 1,
    'one': 1,
    'double': 2,
    'two': 2,
    'triple': 3,
    'three': 3,
    'four': 4,
    'five': 5,
    'six': 6,
    'seven': 7,
    'eight': 8,
    'nine': 9,
    'ten': 10,
    'eleven': 11,
    'twelve': 12
}

def to_float(complex_fraction):
    """Return a complicated number as a single float.

    Input can be a space seperated mixture of integers, floats, and fractions.
    For example,

        >>> from sfss.utils import to_float
        >>> s = '3.2 1/2'
        >>> f = to_float(s)
        >>> f, type(f)
        (3.7, <type 'float'>)
    """
    def tof(nstr):
        return float(fractions.Fraction(nstr))
    return sum(map(tof, complex_fraction.split()))

def get_all_items(filename):
    with open(filename) as f:
        return json.loads(f.read())

def verify_files_exist(jsonpath):
    """Read data at jsonpath to check each file mentioned actually exists."""
    base = os.path.dirname(jsonpath)
    paths = []
    data = None
    with open(jsonpath) as f:
        data = json.loads(f.read())
    if not data:
        raise ValueError('No data found')
    for d in data:
        for k in ['files', 'images']:
            for info in d.get(k, []):
                if 'path' in info:
                    abspath = os.path.join(base, k, info['path'])
                    paths.append(abspath)
    #paths = [os.path.join(base, p) for p in paths]
    for p in paths:
        if not os.path.isfile(p):
            print "Couldn't find %r" % p



# Spec analysis helpers
# -----------------------------

def _get_class_spec():
    """Reads and returns the class specs as data."""
    here = os.path.dirname(__file__)
    fpath = os.path.join(here, '../scrape_data/specs/classes')
    with open(fpath) as f:
        data = json.loads(f.read())
    return data

def _get_class_analysis(rev=False):
    """Return an analysis of the attributes in the class spec.

    The analysis contains all attribute values found for each attribute type and
    code (regardless of in which class the value was specified).
    """
    data = _get_class_spec()
    d = defaultdict(list)
    for klass in data:
        if not data[klass]:
            continue
        attrs = data[klass].get('Attributes', {})
        for code, spec in attrs.items():
            k = (spec['type'], code) if rev else (code, spec['type'])
            d[k].append(klass)
    return d

def print_class_analysis(rev=False):
    """Print an analysis of the class specs.

    This is most useful when you are trying to ensure attribute value
    consistency across multiple classes.
    """
    d = _get_class_analysis(rev)
    order = sorted(d)
    f = '%s - %s'
    m = max(map(lambda o: len(f % o), order))
    for k in order:
        kk = f % k
        v = ', '.join(d[k])
        n = len(d[k])
        print '{:>3} | {:{w}} | {!s}'.format(n, kk, v, w=m)

def _get_attribute_analysis(code):
    data = _get_class_spec()
    d = defaultdict(set)
    for klass in data:
        if not data[klass]:
            continue
        attrs = data[klass].get('Attributes', {})
        spec = attrs.get(code)
        if not spec:
            continue
        d['types'].add(spec['type'])
        if spec['type'] == 'option':
            d['options'].update(spec['options'])
    return d

def print_attribute_analysis(code=None):
    d = _get_attribute_analysis(code)
    print code
    print '  %d type(s):' % len(d['types'])
    for t in d['types']:
        print '    - %s' % t
    if 'options' in d:
        print '  %d option(s):' % len(d['options'])
        for o in d['options']:
            print '    - %s' % o


# Helper functions for processing UPCs and their differences
# --------------------------------------------------------------

def get_features(upc, parent=None, features=None, process_parents=True):
    feature_count = Counter()
    if parent is None and not process_parents:
        feature_count[upc] = 1
    else:
        if features is None:
            features = get_features.defaults
            if not process_parents:
                features.insert(0, parent)
        for key in features:
            if key.startswith('.'):
                key = key[1:]
                i = upc.rfind(key)
            else:
                i = upc.find(key)
            if i < 0:
                continue
            upc = upc[:i] + upc[i + len(key):]
            feature_count[key] += 1
    return feature_count
get_features.defaults = ['PRO', '.TH', '.PH', '.LH', '.RH', 'UC', 'FS',
                        '.LP', '.HA', '.I', '.P', '.A']

def get_upc_diff(upc, parent, features=None):
    pf = get_features(parent, features=features)
    cf = get_features(upc, parent, features=features)
    return {k:v for k, v in (cf - pf).items() if v > 0}

def get_all_upc_diffs(data, features=None):
    diffs = {}
    for item in data:
        parent = item['upc']
        diffs[parent] = {}
        children = item.get('children', [])
        for child in children:
            child_diff = get_upc_diff(child, parent, features=features)
            diffs[parent][child] = child_diff
    return diffs

def get_all_unique_upc_diffs(data, features=None):
    record = Counter()
    for item in data:
        parent = item['upc']
        children = item.get('children', [])
        for child in children:
            diff = get_upc_diff(child, parent, features=features)
            record.update(diff)
    return record
