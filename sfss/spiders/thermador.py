# -*- coding: utf-8 -*-
import scrapy
import re
from scrapy.utils.response import get_base_url
from scrapy.utils.url import urljoin_rfc
from scrapy.utils.url import url_query_parameter
from urlparse import urlparse

from sfss import utils
from sfss.items import ProductItem
from fractions import Fraction

CAPACITY_RE = (r'^.*(cavity at )? (\d+(\.\d+)?) (combined )?(cubic|cu\.)'
               r' ?(feet|foot|ft\.)( of)? ?((oven )?capacity|cavity|'
               r'stainless steel cavity)?.*$')


class ThermadorSpider(scrapy.Spider):
    name = "Thermador"
    allowed_domains = ["thermador.com",
                       "rackcdn.com",
                       "portal.bsh-partner.com"]
    start_urls = (
        'http://www.thermador.com/sitemap',
    )

    def parse(self, response):
        url_xpath = '//div[@class="contentWrap"]/div/div/ul/li/a/@href'
        urls = response.xpath(url_xpath).extract()

        url_regex = r'^\/(cooking|refrigeration|dishwashers)\/'
        urls = [x for x in urls if re.match(url_regex, x)]

        for url in urls:
            url = urljoin_rfc(get_base_url(response), url)
            request = scrapy.Request(url,
                                     callback=self.parse_category)
            yield request

    def parse_category(self, response):
        urls = response.css('.productEntry > a::attr(href)').extract()
        for url in urls:
            url = urljoin_rfc(get_base_url(response), url)
            request = scrapy.Request(url,
                                     callback=self.parse_product)
            yield request

    def parse_product(self, response):
        item = ProductItem()
        item['title'] = response.css('.pageHeader > h1::text')[0].extract()
        item['url'] = response.url
        item['maker'] = 'Thermador'
        item['upc'] = response.css('span[itemprop="sku"]::text')[0].extract()

        blurb = response.css('span[itemprop="name"]::text').extract()
        item['blurb'] = ''.join(blurb)

        images_css = 'div.rightPhoto img::attr(src)'
        images = response.css(images_css).extract()
        images = [urljoin_rfc(get_base_url(response), x)
                  for x in images if 'energyStarLogo' not in x]
        item['image_urls'] = images

        desc = response.css('span[itemprop="description"] > p::text').extract()
        desc = response.css('meta[name="description"]::attr(content)').extract()
        item['description'] = u'\n'.join(desc)

        cats = response.css('.breadcrumb > a::text').extract()
        item['categories'] = [u' > '.join(cats)]

        classes = {
            'Ranges': ('Range', self.handle_range),
            'Rangetops': ('Rangetop', self.handle_rangetop),
            'Ovens': ('Oven', self.handle_oven),
            'Cooktops': ('Cooktop', self.handle_cooktop),
            'Ventilation': ('Ventilation', self.handle_ventilation),
            'Warming Drawers': ('Warming Drawer', self.handle_warming_drawer),
            'Microwaves': ('Microwave', self.handle_microwave),
            'Built-In Coffee Machine': ('Coffee Maker',
                                        self.handle_coffee_maker),
            'Refrigerator Columns': ('Refrigeration',
                                     self.handle_refrigeration),
            'Freezer Columns': ('Refrigeration', self.handle_refrigeration),
            'Bottom Freezer Refrigerators': ('Refrigeration',
                                             self.handle_refrigeration),
            'Wine Columns': ('Wine Storage', self.handle_wine_storage),
            'Dishwashers': ('Dishwasher', self.handle_dishwasher),
        }
        item['class_'] = classes[cats[-1]][0] if cats[-1] in classes else '?'
        class_handler = classes[cats[-1]][1] if cats[-1] in classes else None

        f_items = response.css('h4:contains("View all Features") + .group')
        f_names = f_items.css('strong::text').extract()
        f_descriptions = []
        for list_item in f_items.css('ul'):
            f_descriptions.append(list_item.css('li::text').extract())
        item['features'] = dict(zip(f_names, f_descriptions))

        item['attributes'] = {}
        # Grab the width
        width = self._get_width(item, response)
        item['attributes']['width'] = 'Width', width

        all_dims = self._get_dims(item, response, dims='whd')
        dims_str = []
        for dim in all_dims:
            if dim.group(1).lower() == 'w' and not width:
                item['attributes']['width'] = 'Width', dim.group(2) + '"'
            dims_str.append('{}"{}'.format(dim.group(2), dim.group(1)))
        if dims_str:
            dimstr = ' x '.join(dims_str)

            item['attributes'].update({
                'dimensions': ('Dimensions', dimstr)
            })

        item['colors'] = []

        # Is Basketable?
        item['is_basketable'] = False

        # Is Price Viewable?
        item['is_price_viewable'] = False

        item['file_urls'], ext_url = self.process_downloads(response)

        if class_handler:
            class_handler(item, response, None)

        if ext_url:
            query_param = url_query_parameter(ext_url, 'MATNRGA') + '*'
            ext_url = ('https://portal.bsh-partner.com/TCcustomBSH/'
                       'controller/get_attached_pdf')
            data = {
                'PORTAL_REGIONINDEX': '0000000020',
                'PORTAL_LAYOUTINDEX': '0000000012',
                'PORTAL_LANGUAGE': 'EN',
                'PORTAL_USER_SITE': '',
                'b01EMatNr': query_param,
                'backpage': '/service12(bD1lbg==)/instruction_manual_new.htm',
            }
            request = scrapy.FormRequest(ext_url,
                                         formdata=data,
                                         callback=self.parse_external_downloads)
            request.meta['item'] = item
            yield request
        else:
            yield item

    @staticmethod
    def _get_width(item, response):
        title_without_depth = re.sub(r'^.*(\d+(\.\d+)?) ' +
                                     r'(inch(es)|")? (depth|deep).*$',
                                     '', item['title'].lower())
        inch_match = re.match(r'^.* (\d+(\.\d+)?) inch(es)?\b.*$',
                              title_without_depth)
        if inch_match:
            return inch_match.group(1) + u'"'
        elif '"' in title_without_depth:
            inches, _ = title_without_depth.split('"', 1)
            try:
                return inches + u'"'
            except Exception:
                pass
        elif ' inch ' in title_without_depth.lower():
            inches, _ = title_without_depth.lower().split(' inch ', 1)
            try:
                return inches + u'"'
            except Exception:
                pass
        elif 'inch' in response.url:
            try:
                _, last = response.url.rsplit('/', 1)
                inches, _ = last.split('-inch')
                _, inches = inches.split('-')
                return inches + u'"'
            except Exception:
                pass
        elif 'warming drawer' in item['title'].lower():
            return u'30"'
        elif 'pro 48' in item['title'].lower():
            return u'48"'
        return 'See Spec Sheet'

    def parse_external_downloads(self, response):
        item = response.meta['item']
        file_urls = item['file_urls']
        file_sizes = []

        for row in response.css('#datalist > tbody > tr'):
            size = row.css('td:nth-child(3) *::text').extract()[0]
            languages = ' '.join(row.css('td:nth-child(2) *::text').extract()).lower()
            if size not in file_sizes and 'en' in languages:
                url = row.css('td > a::attr(href)')[0].extract()
                url = urljoin_rfc(get_base_url(response), url)
                name = row.css('td > a::text')[0].extract()
                kind = row.css('td > span::attr(title)')[-1].extract()
                file_urls[url] = {'name': name, 'kind': get_kind(kind)}
                file_sizes.append(size)

        for spec_link in response.css('.specsTable a'):
            url = spec_link.css('::attr(href)')[0].extract()
            name = spec_link.css('::text')[0].extract()
            file_urls[url] = {'name': name, 'kind': get_kind(name)}
        yield item

    def process_downloads(self, response):
        tab_css = 'li > a:contains("Manuals & CAD Files")::attr(rel)'
        tab = response.css(tab_css)[0].extract()
        links = response.css('{0} a'.format(tab))

        file_urls = {}
        ext_url = None
        for link in links:
            url = link.css('::attr(href)').extract()[0]
            if urlparse(url).netloc == u'portal.bsh-partner.com':
                ext_url = url
            else:
                name = link.css('::text').extract()[0]
                file_urls[url] = {'name': name, 'kind': get_kind(name)}
        return file_urls, ext_url

    @classmethod
    def handle_range(cls, item, response, children):

        ltitle = item['title'].lower()

        # Fuel
        if re.match(r'^.*\bdual fuel\b.*$', ltitle):
            fuel = 'Dual Fuel'
        elif re.match(r'^.*\bgas\b.*$', ltitle):
            fuel = 'Gas'
        else:
            fuel = 'Electric'

        # Options
        has_electric_griddle = False
        has_ir_griddle = re.match(r'^.*\binfrared griddle\b.*$',
                                  ltitle) is not None
        has_ir_charbroiler = re.match(r'^.*\binfrared charbroiler\b.*$',
                                      ltitle) is not None
        has_french_top = re.match(r'^.*\bfrench top\b.*$', ltitle) is not None

        secs_css = 'span[itemprop="description"],.featuresAccordian,.specsTable'
        texts = response.css(secs_css).css('::text').extract()
        text = ' '.join(texts)
        ltext = text.lower()
        ltext = re.sub(r'\s+', ' ', ltext)

        sabbath_mode = re.match(r'^.*\bstar( |\-)k\b.*$', ltext) is not None
        sabbath_compliance = "Yes" if sabbath_mode else "No"
        uses_convection = re.match(r'^.*\bconvection\b.*$', ltext) is not None
        self_cleaning = re.match(r'^.*\b(self clean|self-clean)\b.*$',
                                 ltext) is not None
        delayed_baking = re.match(r'^.*\bdelayed start\b.*$', ltext) is not None

        elements = cls._get_element_count(item, response)

        item['attributes'].update({
            'type': ('Type', 'Freestanding'),
            'fuel': ('Fuel', fuel),
            'has_delay_bake': (
                'Oven Features::Delayed Baking', delayed_baking),
            'element_count': ('Burners/Elements', elements),
            'uses_convection': ('Oven Features::Convection', uses_convection),
            'drawer': (
                'Oven Features::Drawer Type', 'None'),
            'has_self_clean': (
                'Oven Features::Self Cleaning', self_cleaning),
            'has_electric_griddle': (
                'Rangetop Features::Electric Griddle', has_electric_griddle),
            'has_ir_griddle': (
                'Rangetop Features::Infrared Griddle', has_ir_griddle),
            'has_ir_charbroiler': (
                'Rangetop Features::Infrared Charbroiler', has_ir_charbroiler),
            'has_french_top': (
                'Rangetop Features::French Top', has_french_top),
            'has_sabbath_mode': (
                'Star-K Compliance::Sabbath Mode', sabbath_mode),
            'is_sabbath_compliant': (
                'Star-K Compliance::Sabbath Compliant', sabbath_compliance),
        })

    @staticmethod
    def _get_element_count(item, response, depth=1):
        burners = None
        product_title = item['title'].lower()
        # Method one: split the title.
        secs_css = 'span[itemprop="description"],.featuresAccordian,.specsTable'
        candidates = response.css(secs_css).css('::text')
        likely_text = ' '.join(candidates.extract()).lower()
        likely_text = re.sub(r'\s+', ' ', likely_text)

        burners_re = (ur'^.+ (\d|one|two|three|four|five)'
                      ur' ((pedestal )?star® )?burners? .+$')
        b_match = re.match(burners_re, product_title)
        if not b_match:
            b_match = re.match(burners_re, likely_text)
        if b_match:
            numbers = {
                'one': 1,
                'two': 2,
                'three': 3,
                'four': 4,
                'five': 5
            }
            try:
                burners = int(b_match.group(1))
            except ValueError:
                burners = numbers[b_match.group(1)]

        if not burners:
            # Method two: search the features.
            count_re = r'\d+|%s' % '|'.join(utils.numbers)
            target_re = r'(element|burner)s'
            extra_re = r'(\s+[\w-]+){0,%d}' % depth

            pattern_ = r'(?P<n>%s)%s\s+%s'
            pattern = pattern_ % (count_re, extra_re, target_re)
            regex = re.compile(pattern, re.I)

            # Look for the mentioning of the burner/element count that is the
            # greatest.
            greatest = 0
            for card, _, _ in regex.findall(likely_text):
                try:
                    card = int(card)
                except ValueError:
                    card = utils.numbers.get(card, 0)
                if card > greatest:
                    greatest = card
            if greatest:
                burners = greatest

        if not burners:
            # Method three: disect the UPC.
            # Some cooktop UPCs contain the width followed by the number of
            # elements/burners (e.g., 30" w/2 burners might be 'CT302S/T').
            try:
                width = str(item['attributes']['width'][1])
                width = re.sub(r'"$', '', width)
                w = str(width)
                i = item['upc'].index(w) + len(w)
                burners = int(item['upc'][i])
            except Exception:
                pass

        if not burners:
            # Method four: base it on the width!
            # A loose relationship exists between the width of the cooktop and
            # the number of burners/elements:
            #   - 15" -> 2
            #   - 30" -> 4
            #   - 36" -> 5
            try:
                width = str(item['attributes']['width'][1])
                burners = {'15"': 2, '30"': 4, '36"': 5}.get(width)
            except Exception:
                pass

        if not burners:
            # Method four: just give up and guess two
            burners = 2
        return burners

    @classmethod
    def handle_rangetop(cls, item, response, children):

        ltitle = item['title'].lower()
        secs_css = 'span[itemprop="description"],.featuresAccordian,.specsTable'
        texts = response.css(secs_css).css('::text').extract()
        text = ' '.join(texts)
        ltext = text.lower()
        ltext = re.sub(r'\s+', ' ', ltext)

        has_electric_griddle = False
        has_ir_griddle = 'infrared griddle' in ltitle
        has_ir_charbroiler = 'infrared charbroiler' in ltitle
        has_french_top = 'french top' in ltitle
        sabbath_mode = re.match(r'^.*\bstar( |\-)k\b.*$', ltext) is not None
        sabbath_compliance = "Yes" if sabbath_mode else "No"
        has_grill = 'grill' in ltitle
        fits_wok = len(re.findall(r'\bwok\b', ltext)) > 0
        natural_gas = 'natural gas' in ltext

        elements = cls._get_element_count(item, response)

        item['attributes'].update({
            'type': ('Type', 'Built-in'),
            'fuel': ('Fuel', 'Gas'),
            'burner_type': ('Burner Type', 'Sealed'),
            'fuel_type': ('Fuel Type', 'NG' if natural_gas else 'LP'),
            'element_count': ('Burners/Elements', elements),
            'has_ir_charbroiler': ('Infrared Charbroiler', has_ir_charbroiler),
            'has_ir_griddle': ('Infrared Griddle', has_ir_griddle),
            'has_electric_griddle': ('Electric Griddle', has_electric_griddle),
            'has_grill': ('Grill', has_grill),
            'has_french_top': ('French Top', has_french_top),
            'has_wok': ('Fits Wok', fits_wok),
            'has_sabbath_mode': (
                'Star-K Compliance::Sabbath Mode', sabbath_mode),
            'is_sabbath_compliant': (
                'Star-K Compliance::Sabbath Compliant', sabbath_compliance),
        })

    @classmethod
    def handle_oven(cls, item, response, children):
        ltitle = item['title'].lower()

        # design_style
        if 'transitional' in ltitle:
            design_style = 'Transitional'
            finish = 'Stainless Steel'
        elif 'professional' in ltitle:
            design_style = 'Professional'
            finish = 'Stainless Steel'
        elif 'contemporary' in ltitle:
            design_style = 'Standard'
            finish = 'Color'
            item['colors'].append({
                'name': 'Black',
                'hexcode': '000000'
            })
        else:  # if 'l series' in ltitle:
            design_style = 'None'
            finish = 'Stainless Steel'

        secs_css = 'span[itemprop="description"],.featuresAccordian,.specsTable'
        texts = response.css(secs_css).css('::text').extract()
        text = ' '.join(texts)
        ltext = text.lower()
        ltext = re.sub(r'\s+', ' ', ltext)

        # Capacity
        capacity = None

        cap_match = re.match(CAPACITY_RE, ltext)
        if cap_match:
            capacity = float(cap_match.group(2))

        # Gas or electric (yes, gas ovens exist, apparently).
        natural_gas = 'natural gas' in ltext
        if 'gas' in ltitle:
            fuel = 'Gas'
            fuel_type = 'NG' if natural_gas else 'LP'
        else:
            fuel = 'Electric'
            fuel_type = 'None'

        # Configuration
        if 'double' in ltitle:
            configuration = 'Double'
        elif 'triple' in ltitle:
            configuration = 'Triplestack'
        else:
            configuration = 'Single'

        # Type
        type_ = 'All Oven'

        # Convection or not.
        if 'convection' in ltitle:
            uses_convection = True
        else:
            uses_convection = 'convection' in ltext

        # Method
        if 'combi' in ltitle:
            method = 'Steam'
        elif 'steam' in ltitle:
            method = 'Steam'
        elif 'speed' in ltitle:
            method = 'Speed'
        else:
            method = 'Heat'

        # Outdoor
        is_outdoor = False

        # Self-cleaning
        self_cleaning = 'self clean' in ltext or 'self-clean' in text

        item['attributes'].update({
            'fuel': ('Fuel', fuel),
            'fuel_type': ('Fuel Type', fuel_type),
            'design_style': ('Design Style', design_style),
            'finish': ('Finish', finish),
            'method': ('Method', method),
            'configuration': ('Configuration', configuration),
            'capacity': ('Capacity', capacity),
            'uses_convection': ('Convection', uses_convection),
            'type': ('Type', type_),
            'has_self_clean': ('Self Cleaning', self_cleaning),
            'is_outdoor': ('Outdoor', is_outdoor),
        })

    @classmethod
    def handle_cooktop(cls, item, response, children):
        ltitle = item['title'].lower()

        secs_css = 'span[itemprop="description"],.featuresAccordian,.specsTable'
        texts = response.css(secs_css).css('::text').extract()
        text = ' '.join(texts)
        ltext = text.lower()
        ltext = re.sub(r'\s+', ' ', ltext)

        finish = 'Stainless Steel'  # All cooktops finished w/ stainless steel.
        fuel = 'Gas' if 'gas' in ltitle else 'Electric'
        fuel_type = 'Liquid Propane' if fuel == 'Gas' else 'None'
        if 'natural gas' in ltext:
            fuel_type = 'Natural Gas'
        uses_induction = 'induction' in ltitle
        is_framed = 'unframed' not in ltitle and 'unframed' not in ltext
        has_smoothtop = 'smoothtop' in ltext

        elements = cls._get_element_count(item, response)

        item['attributes'].update({
            'finish': ('Finish', finish),
            'fuel': ('Fuel', fuel),
            'fuel_type': ('Fuel Type', fuel_type),
            'uses_induction': ('Induction', uses_induction),
            'element_count': ('Burners/Elements', elements),
            'is_framed': ('Framed', is_framed),
            'has_smoothtop': ('Smoothtop', has_smoothtop),
        })

    @classmethod
    def handle_ventilation(cls, item, response, children):
        ltitle = item['title'].lower()

        secs_css = 'span[itemprop="description"],.featuresAccordian,.specsTable'
        texts = response.css(secs_css).css('::text').extract()
        text = ' '.join(texts)
        ltext = text.lower()
        ltext = re.sub(r'\s+', ' ', ltext)

        type_ = ""
        is_wall_mount = 'is mounted on roof or exteriro wall' in ltext
        is_wall_mount = is_wall_mount or 'wall hood' in ltitle
        is_wall_mount = is_wall_mount or 'wall chimney' in ltitle
        is_insert = 'inline' in ltitle or 'hood liner' in ltitle
        is_insert = is_insert or 'hood insert' in ltitle
        is_insert = is_insert or 'hood insert' in ltext
        is_insert = is_insert or 'custom insert' in ltitle
        if is_wall_mount:
            type_ = 'Wall Mount'
        elif 'island hood' in ltitle:
            type_ = 'Island'
        elif 'downdraft' in ltitle:
            type_ = 'Downdraft'
        elif is_insert:
            type_ = 'Insert'
        elif 'under' in ltitle and 'cabinet' in ltitle:
            type_ = 'Under Cabinet'

        if type_ == 'Insert':
            finish = 'Insert'
        else:
            finish = 'Stainless Steel'  # assumption
            if 'black' in ltitle:
                finish = 'Color'
                item['colors'].append({
                    'name': 'Black',
                    'hexcode': '000000'
                })

        venting = 'Ducted'
        if 'retractable' in ltext:
            movement = 'Retractable'
        elif re.search(r'\b(rise|lower)s\b', ltext):
            movement = 'Ascending'
        else:
            movement = 'Fixed'

        item['attributes'].update({
            'type': ('Type', type_),
            'finish': ('Finish', finish),
            'venting': ('Venting', venting),
            'movement': ('Movement', movement),
        })

    @classmethod
    def handle_warming_drawer(cls, item, response, children):
        ltitle = item['title'].lower()

        secs_css = 'span[itemprop="description"],.featuresAccordian,.specsTable'
        texts = response.css(secs_css).css('::text').extract()
        text = ' '.join(texts)
        ltext = text.lower()
        ltext = re.sub(r'\s+', ' ', ltext)

        is_cup_and_plate = 'cup warming' in ltitle
        is_outdoor = 'outdoor' in ltitle

        if 'black' in ltitle:
            finish = 'Color'
            item['colors'].append({
                'name': 'Black',
                'hexcode': '000000'
            })
        else:
            finish = 'Stainless Steel'

        sabbath_mode = 'star k' in ltext
        uses_convection = 'convection' in ltext

        # Capacity
        capacity = None

        cap_match = re.match(CAPACITY_RE, ltext)
        if cap_match:
            capacity = float(cap_match.group(2))

        item['attributes'].update({
            'is_cup_plate': ('For Cups and Plates', is_cup_and_plate),
            'is_outdoor': ('Outdoor', is_outdoor),
            'finish': ('Finish', finish),
            'has_sabbath_mode': ('Sabbath Mode', sabbath_mode),
            'uses_convection': ('Convection', uses_convection),
            'capacity': ('Capacity', capacity),
        })

    @classmethod
    def handle_microwave(cls, item, response, children):
        ltitle = item['title'].lower()
        uses_convection = 'convection' in ltitle
        attrs = response.css('.specsTable td::text').extract()
        attrs = [attrs[n:n + 2] for n in range(0, len(attrs), 2)]

        # Type and style.
        if 'built' in ltitle:
            type_ = 'Built-in'
        else:
            type_ = 'Freestanding'

        if 'drawer' in ltitle:
            style = 'Drawer'
        else:
            style = 'Door'

        secs_css = 'span[itemprop="description"],.featuresAccordian,.specsTable'
        texts = response.css(secs_css).css('::text').extract()
        text = ' '.join(texts)
        ltext = text.lower()
        ltext = re.sub(r'\s+', ' ', ltext)

        # Power
        wattage = None
        matches = re.findall(r'(\d+)\s+watts', ltext, re.I)
        try:
            wattage = max(map(int, matches))
        except ValueError:
            pass

        # Design style
        if 'transitional' in ltitle:
            design_style = 'Transitional'
        elif 'professional' in ltitle:
            design_style = 'Professional'
        else:
            design_style = 'Standard'

        # Capacity
        capacity = None
        cap_match = re.match(CAPACITY_RE, ltext)
        if cap_match:
            capacity = float(cap_match.group(2))

        # Turntable
        turntable = False
        for name, value in attrs:
            if name.lower() == "turntable":
                turntable = value.lower() == "yes"

        item['attributes'].update({
            'style': ('Style', style),
            'design_style': ('Design Style', design_style),
            'type': ('Type', type_),
            'capacity': ('Capacity', capacity),
            'convection': ('Convection', uses_convection),
            'has_turntable': ('Has Turntable', turntable),
            'wattage': ('Wattage', wattage),
        })

    def handle_coffee_maker(cls, item, response, children):
        item['class_'] = 'Coffee Maker'
        # item['categories'] = [
        #     'Kitchen > Cooking > Built-in Coffee Makers',
        #     ]

        # All subzero coffee makers have the following attribute values.
        item['attributes']['type'] = 'Type', 'Built-in'
        item['attributes']['is_plumbed'] = 'Plumbed', False
        item['attributes']['is_programmable'] = 'Programmable', True
        item['attributes']['dispenses_hot_water'] = 'Hot Water Dispenser', True
        item['attributes']['froths_milk'] = 'Milk Frother', True
        item['attributes']['grinds_beans'] = 'Bean Grinder', True
        item['attributes']['carafe_capacity'] = 'Carafe Capacity (oz)', 12

        # Subzero coffee makers are either stainless steel or black.
        if 'Stainless' in item['title']:
            item['attributes']['finish'] = 'Finish', 'Stainless Steel'
        else:
            item['attributes']['finish'] = 'Finish', 'Color'
            item['colors'].append({'name': 'Black', 'hexcode': '#000000'})

    @classmethod
    def handle_refrigeration(cls, item, response, children):
        # Default values
        configuration = 'Column'
        type_ = 'Freestanding'
        function = 'All Refrigerator'
        shape = 'Upright'
        finish = None
        hinge = None
        is_wine_storage = None
        is_outdoor = False
        is_undercounter = False
        is_counter_depth = False
        makes_ice = False

        # Things we will use repeatedly to make determinations.
        ltitle = item['title'].lower()
        url = urlparse(response.url)

        is_outdoor = False
        if 'outdoor' in ltitle:
            is_outdoor = True
            # item['categories'].append(
            #     'Outdoor Living > Refrigeration > Refrigerators & Freezers'
            # )

        is_undercounter = '/counter-refrigerator/' in url.path

        is_counter_depth = True

        secs_css = 'span[itemprop="description"],.featuresAccordian,.specsTable'
        texts = response.css(secs_css).css('::text').extract()
        text = ' '.join(texts)
        ltext = text.lower()
        ltext = re.sub(r'\s+', ' ', ltext)

        # Ice maker?
        makes_ice = 'ice-maker' in ltext or 'ice system' in ltext

        # Does it mention wine racks, wine storage, or wine shelves?
        n = len(re.findall(r'wine\s+(storage|rack|shel[vf])', ltext))
        is_wine_storage = bool(n)

        # Type and finsih (only the Pro48 series is freestanding)
        if 'pro 48' in ltitle:
            type_ = 'Freestanding'
            finish = 'Stainless Steel'
            configuration = 'Side-by-side'
            function = 'Refrigerator & Freezer'
            shape = 'Upright'
        else:
            type_ = 'Built-in'

            # Finish
            if 'integrated-fridges' in url.path:
                finish = 'Integrated'
            elif 'panel ready' in ltitle:
                finish = 'Panel-ready'
            else:
                finish = 'Stainless Steel'

            # Configuration
            if 'side-by-side' in ltitle:
                configuration = 'Side-by-side'
            elif 'over-under' in ltitle or 'over-and-under' in ltitle:
                configuration = 'Bottom-mount'
            elif 'french door' in ltitle:
                configuration = 'French Door'
            elif 'column' in ltitle:
                configuration = 'Column'
            else:
                configuration = 'Single'

            # Function (aka make up)
            if 'freezer' in ltitle:
                if 'refrigerator' in ltitle:
                    function = 'Refrigerator & Freezer'
                else:
                    function = 'All Freezer'
            else:
                function = 'All Refrigerator'

            # Shape
            if 'drawers' in ltitle:
                shape = 'Drawer'
            else:
                shape = 'Upright'

        hinge_match = re.match(r'^.*reversible door hinge ?yes.*$', ltext)
        if hinge_match:
            hinge = 'Both'
        if not hinge and u'Freedom® Hinge'.lower() in ltext:
            hinge = 'Both'
            # "Left-hand",
        # "Right-hand",

        item['attributes'].update({
            'type': ('Type', type_),
            'function': ('Function', function),
            'shape': ('Shape', shape),
            'configuration': ('Configuration', configuration),
            'finish': ('Finish', finish),
            'is_outdoor': ('Outdoor', is_outdoor),
            'is_undercounter': ('Undercounter', is_undercounter),
            'is_counter_depth': ('Counter Depth', is_counter_depth),
            'is_wine_storage': ('Stores Wine', is_wine_storage),
            'makes_ice': ('Makes Ice', makes_ice),
            'hinge': ('Hinge', hinge),
        })

    @classmethod
    def handle_wine_storage(cls, item, response, children):
        specs = response.css(".specsTable *::text").extract()
        specs = '\n'.join([x for x in specs if len(x.strip())])

        # Figure out type
        ltitle = item['title'].lower()
        type_ = "Built-in"
        if 'freestanding' in ltitle:
            type_ = 'Freestanding'

        secs_css = 'span[itemprop="description"],.featuresAccordian,.specsTable'
        texts = response.css(secs_css).css('::text').extract()
        text = ' '.join(texts)
        ltext = text.lower()
        ltext = re.sub(r'\s+', ' ', ltext)

        # Figure out bottle capacity
        bottle_capacity = 'See Spec Sheet'
        bottle_re = re.compile('(\d+)\s+Bottles', re.I)
        try:
            bottle_capacity = int(bottle_re.findall(ltext)[0])
        except IndexError:
            pass

        # Figure out number of temperature zones
        temperature_zones = 1
        depth = 3
        count_re = r'\d+|%s' % '|'.join(utils.numbers)
        target_re = '(temperature zone)'
        extra_re = r'(\s+[\w-]+){0,%d}' % depth
        pattern_ = r'(?P<n>%s)%s\s+%s'
        pattern = pattern_ % (count_re, extra_re, target_re)
        regex = re.compile(pattern, re.I)

        likely_text = ltext
        greatest = 0
        for card, _, _ in regex.findall(likely_text):
            try:
                card = int(card)
            except ValueError:
                card = utils.numbers.get(card, 0)
            if card > greatest:
                greatest = card
        if greatest:
            temperature_zones = greatest

        # Figure out configuration
        configuration = "Single"
        if 'column' in ltitle:
            configuration = "Column"

        # Figure out finish (the order of the if statements is important)
        finish = "Stainless Steel"
        if "panel ready" in ltitle:
            finish = "Panel-ready"

        if "integrated" in ltitle:
            finish = "Integrated"

        # Figure out is_outdoor
        # SubZero wine storage is always indoor
        is_outdoor = False

        # Figure out is_undercounter
        is_undercounter = False
        if "undercounter" in ltitle:
            is_undercounter = True

        # Figure out is_counter_depth
        is_counter_depth = True

        # Figure out if its energy star rated or not
        energy_star_rated = False
        if "energy star certified" in specs.lower():
            energy_star_rated = True

        # Figure out if its energy star rated or not
        sabbath_compliance = "No"
        sabbath_mode = False
        if "star k certified" in specs.lower():
            sabbath_compliance = "Yes"
            sabbath_mode = True
        hinge = None
        hinge_match = re.match(r'^.*reversible door hinge ?yes.*$', ltext)
        if hinge_match:
            hinge = 'Both'
        if not hinge and u'Freedom® Hinge'.lower() in ltext:
            hinge = 'Both'
            # "Left-hand",
        # "Right-hand",

        item['attributes'].update({
            'type': ('Type', type_),
            'bottle_capacity': ('Bottle Capacity', bottle_capacity),
            'temperature_zones': ('Temperature Zones', temperature_zones),
            'configuration': ('Configuration', configuration),
            'finish': ('Finish', finish),
            'is_outdoor': ('Outdoor', is_outdoor),
            'is_undercounter': ('Undercounter', is_undercounter),
            'is_counter_depth': ('Counter Depth', is_counter_depth),
            'is_energy_rated': ('Energy-Star Rated', energy_star_rated),
            'has_sabbath_mode': ('Sabbath Mode', sabbath_mode),
            'is_sabbath_compliant': ('Sabbath Compliance', sabbath_compliance),
            'hinge': ('Hinge', hinge),
        })

    @classmethod
    def handle_dishwasher(cls, item, response, children):

        specs = response.css("#specs *::text").extract()
        specs = '\n'.join([x for x in specs if len(x.strip())])

        # Figure out type
        ltitle = item['title'].lower()
        type_ = 'Freestanding'
        if 'buil-in' in response.url:
            type_ = "Built-in"
        if 'integrated' in ltitle:
            type_ = 'Integrated'

        # Figure out finish (the order of the if statements is important)
        finish = "Stainless Steel"
        if "panel ready" in ltitle:
            finish = "Panel-ready"

        if "integrated" in ltitle:
            finish = "Integrated"

        if 'black' in ltitle:
            finish = 'Color'
            item['colors'].append({
                'name': 'Black',
                'hexcode': '000000'
            })

        #Number of programs
        num_programs = None
        pr_match = re.search(r'\b(\d) programs\b', ltitle)
        if pr_match:
            num_programs = int(pr_match.group(1))

        secs_css = 'span[itemprop="description"],.featuresAccordian,.specsTable'
        texts = response.css(secs_css).css('::text').extract()
        text = ' '.join(texts)
        ltext = text.lower()
        ltext = re.sub(r'\s+', ' ', ltext)

        # Capacity
        capacity = ''

        cap_match = re.match(CAPACITY_RE, ltext)
        if cap_match:
            capacity = float(cap_match.group(2))

        weight = None
        weight_re = r'^.*gross weight \(lbs\) *(\d+(\.\d+)?).*$'
        weight_match = re.match(weight_re, ltext)
        if weight_match:
            weight = weight_match.group(1) + u'lbs'

        item['attributes'].update({
            'type': ('Type', type_),
            'finish': ('Finish', finish),
            'num_programs': ('Number of Programs', num_programs),
            'capacity': ('Capacity', capacity),
            'weight': ('Weight', weight),
        })

    @classmethod
    def _get_dims(cls, item, response, dims='whd', sep='x', unit='"'):
        assert dims
        text = ' '.join(response.css('.tabSection ::text').extract())
        text_lines = [l for l in text.splitlines() if ':' in l]

        # Search and return all results.
        matches = []
        for line in text_lines:
            d_match = re.match(r'^.*(w|h|d)\:\s*(\d+(\s+\d\/\d)?)\s*("|'').*$',
                               line, re.I)
            if d_match:
                matches.append(d_match)
        return matches

    @staticmethod
    def _parse_dimension(dim, unit='"'):
        nstr = None
        if '"' in dim:
            nstr = dim.split(unit, 1)[:1][0]
        else:
            # Read up to the first char that can't be in a legit float.
            i = 0
            while dim[i] in '/ .0123456789':
                i += 1
            nstr = dim[:i]
        try:
            return float(sum(map(Fraction, nstr.split())))
        except Exception:
            raise ValueError('%r cannot be converted into a float' % dim)


def get_kind(name):
    lname = name.lower()
    kind = 'OTH'
    if 'instructions for use' in lname:
        kind = 'UAC'
    elif 'short instructions' in lname:
        kind = 'REF'
    elif 'programme table' in lname:
        kind = 'REF'
    elif 'quick reference' in lname:
        kind = 'REF'
    elif 'installation' in lname:
        kind = 'INS'
    elif 'design guide' in lname:
        kind = 'DES'
    elif 'use and care' in lname:
        kind = 'UAC'
    elif '2d' in lname or 'cad' in lname:
        kind = 'C2D'
    elif '3d' in lname:
        kind = 'C3D'
    elif '20' in lname:
        kind = 'CAT'
    return kind
