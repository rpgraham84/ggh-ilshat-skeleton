# -*- coding: utf-8 -*-
import scrapy
from sfss.items import ProductItem
from scrapy.xlib.tx._newclient import ParseError, HTTPClientParser
import re
from sfss import utils
from sfss.utils.text import slugify


class UlineSpider(scrapy.Spider):
    name = "Uline"
    allowed_domains = ["u-line.com"]
    start_urls = (
        'http://www.u-line.com/?___store=us',
    )

    def __init__(self, *args, **kwargs):
        super(UlineSpider, self).__init__(*args, **kwargs)
        old_sr = HTTPClientParser.statusReceived
        def statusReceived(self, status):
            try:
                return old_sr(self, status)
            except ParseError, e:
                if e.args[0] == 'wrong number of parts':
                    return old_sr(self, status + ' OK')
                raise
        HTTPClientParser.statusReceived = statusReceived

    def parse(self, response):
        url_css = 'a.level3:not(:contains("View All "))::attr(href)'
        urls = response.css(url_css).extract()

        for url in urls:
            url = 'http://www.u-line.com/products/36-custom-models/3000-series/36-glass-door-refrigerator-clear-ice-machine-7368.html?___store=us'
            yield scrapy.Request(url, callback=self.parse_product)

    def parse_product(self, response):
        item = ProductItem()
        title_css = '.product-name > span.h1::text'
        item['title'] = ''.join(response.css(title_css).extract()).strip()
        item['url'] = response.url
        item['maker'] = 'U-Line'

        # Is Basketable?
        item['is_basketable'] = False

        # Is Price Viewable?
        item['is_price_viewable'] = True

        upc_match = re.match(r'^(\w+) .+$', item['title'])
        upcs = []
        if upc_match:
            item['upc'] = upc_match.group(1).strip()
        else:
            upcs_css = '.model-details-section > .title::text'
            upcs = response.css(upcs_css).extract()
        if '/36-custom-models/' in response.url:
            upcs = []
            upc_match = re.match(r'^.+\-(\d+)\.html\?___store=.+$', response.url)
            item['upc'] = None
            if upc_match:
                item['upc'] = upc_match.group(1)

        blurb = response.css('.short-description *::text').extract()
        item['blurb'] = '\n'.join(blurb).strip()

        images_css = '.product-img-gallery img::attr(src)'
        item['image_urls'] = response.css(images_css).extract()

        desc_css = 'meta[name="description"]::attr(content)'
        item['description'] = '\n'.join(response.css(desc_css).extract())

        cats = response.css('.breadcrumbs > ul > li > a')
        cats = [''.join(x.css('*::text').extract()) for x in cats]
        item['categories'] = [u' > '.join(cats[2:-1])]

        classes = {
            u'Beverage Center': ('Wine Storage', self.handle_wine_storage),
            u'Clear Ice Machine': ('Ice Maker', self.handle_ice_maker),
            u'Crescent Ice Maker': ('Ice Maker', self.handle_ice_maker),
            u'Glass Door Refrigerator': ('Refrigeration',
                                         self.handle_refrigeration),
            u'Solid Door Refrigerator': ('Refrigeration',
                                         self.handle_refrigeration),
            u'Solid Refrigerator Drawer': ('Refrigeration',
                                           self.handle_refrigeration),
            u'Refrigerator': ('Refrigeration',
                              self.handle_refrigeration),
            u'Freezer': ('Refrigeration', self.handle_refrigeration),
            u'Combo Model': ('Refrigeration', self.handle_refrigeration),
            u'Refrigerator/Freezer': ('Refrigeration',
                                      self.handle_refrigeration),
            u'Wine Captain Model': ('Wine Storage',
                                    self.handle_wine_storage),
        }
        class_handler = None
        for cla, val in classes.items():
            if cla in item['title']:
                item['class_'] = val[0]
                class_handler = val[1]
                break

        item['price'] = response.css('span.price::text').extract()[0]
        item['price'] = re.sub(r'USD', '', item['price'])

        features = response.css('.product-highlights ~ dd .header-box')
        f_names = features.css('h5::text').extract()
        f_descs = [[''.join(x.css('*::text').extract()) for x in d.css('ul > li')] for d in features]
        item['features'] = dict(zip(f_names, f_descs))

        details = response.css('#product-attribute-specs-table tr')
        details = [[l for l in x.css('*::text').extract() if l.strip()] for x in details]
        skip_atrs = [
            'dimensions-product-depth',
            'dimensions-product-height',
            'dimensions-product-width',
            'sabbath-star-k-certified',
            'energy-star',
        ]
        details = {slugify(x[0]): (x[0], x[1]) for x in details if slugify(x[0]) not in skip_atrs}

        special_features_css = '.special-features-section .content'
        special_features = [''.join(x.css('p::text').extract()) for x in response.css(special_features_css)]
        item['features']['Special Features'] = special_features

        item['attributes'] = details
        item['colors'] = []

        width = self._get_width(item, response)
        item['attributes']['width'] = 'Width', width

        item['attributes'].update({
            'dimensions': ('Dimensions', self._get_dims(item, response))
        })

        item['file_urls'] = self.process_downloads(response)

        models = []
        for table in response.css('.header-table'):
            columns = table.css('th::text').extract()
            for row in table.css('tr'):
                model_list = zip(columns, row.css('td::text').extract())
                model = {k: v for k, v in model_list}
                models.append(model)

        if '/36-custom-models/' in response.url:
            models = []
        upcs_items = []

        for upc in upcs:
            upc_item = ProductItem(**item)
            upc_item['upc'] = upc
            if class_handler:
                class_handler(upc_item, response, None)
            upcs_items.append(upc_item)
        if len(upcs) == 0:
            if class_handler:
                class_handler(item, response, None)
            upcs_items.append(item)

        for item in upcs_items:
            if len(models) > 1:
                upc = item['upc']
                item['upc'] = '(%s)' % upc
                item['children'] = [x[u'Model'] for x in models if upc in x[u'Model']]

                images_alt_css = '.product-img-gallery img::attr(alt)'
                image_urls = item['image_urls']
                image_alts = response.css(images_alt_css).extract()
                img_alt = zip(image_urls, image_alts)

                for model in [m for m in models if upc in m[u'Model']]:
                    child = ProductItem(**item)
                    if 'children' in child:
                        del child['children']
                    child.update({
                        'canonical_upc': item['upc'],
                        'upc': model[u'Model'],
                        'price': model[u'Sug. List'],
                        'attributes': {},
                        'colors': [],
                        'image_urls': [],
                    })

                    finish_text = model[u'Finish'].strip()
                    # set filtered
                    imgs = [i for i, t in img_alt if t == finish_text]
                    child['image_urls'] = imgs
                    finish = None
                    if 'Stainless' in finish_text:
                        finish = "Stainless Steel"

                    if 'Black' in finish_text:
                        finish = "Color"
                        child['colors'].append({
                            'name': 'Black',
                            'hexcode': '#000000'})

                    if "Integrated" in finish_text:
                        finish = "Integrated"
                    if finish:
                        child['attributes'].update({
                            'finish': ('Finish', finish)})

                    hinge = None
                    hinge_text = model[u'Door Swing']

                    if 'Right-Hand Hinge' in hinge_text:
                        hinge = 'Right'
                    if 'Left-Hand Hinge' in hinge_text:
                        hinge = 'Left'
                    if 'Field Reversible' in hinge_text:
                        hinge = 'Both'
                    if hinge:
                        child['attributes'].update({
                            'hinge': ('Hinge', hinge)}
                        )
                    yield child

            yield item

    def _get_width(self, item, response):
        width_match = re.match(ur'^\w+ (\d+)(”|") .+$', item['title'])
        if width_match:
            return width_match.group(1) + u'"'
        width_match = re.match(ur'^(\d+)("|”|\u2033) .+$', item['title'])
        if width_match:
            return width_match.group(1) + u'"'
        return u'"'

    def _get_dims(self, item, response):
        fields = {
            u'D': 'Dimensions: Product Depth',
            u'H': 'Dimensions: Product Height',
            u'W': 'Dimensions: Product Width',
        }
        dims = {}
        for key, value in fields.items():
            f_css = 'th:contains("{0}") ~ td::text'.format(value)
            try:
                text = response.css(f_css).extract()[0]
            except:
                text = ''
                dims = {'D': '0', 'W': '0', 'H': '0'}
            f_m = re.match(ur'^(\d+)((\-?)(\d+\/\d+))?" \/ .+$', text)
            if f_m:
                sfx = f_m.group(4) if f_m.group(4) else ''
                dims[key] = '{} {}'.format(f_m.group(1), sfx).strip()
        return u'{W:s}"W x {H:s}"H x {D:s}"D'.format(**dims)

    def process_downloads(self, response):
        def get_kind(name):
            lname = name.lower()
            kind = 'OTH'
            if 'user guide' in lname:
                kind = 'UAC'
            elif 'quick start guide' in lname:
                kind = 'REF'
            elif 'spec' in lname:
                kind = 'REF'
            elif 'warranty' in lname:
                kind = 'REF'
            elif 'programme table' in lname:
                kind = 'REF'
            elif 'quick reference' in lname:
                kind = 'REF'
            elif 'installation' in lname:
                kind = 'INS'
            elif 'design guide' in lname:
                kind = 'DES'
            elif 'use and care' in lname:
                kind = 'UAC'
            elif '2d' in lname or 'cad' in lname:
                kind = 'C2D'
            elif '3d' in lname:
                kind = 'C3D'
            elif '20' in lname:
                kind = 'CAT'
            return kind
        links = response.css('.downloads-section a')
        file_urls = {}
        for link in links:
            url = link.css('::attr(href)').extract()[0]
            name = link.css('span::text').extract()[0]
            file_urls[url] = {'name': name, 'kind': get_kind(name)}
        return file_urls

    @classmethod
    def handle_wine_storage(cls, item, response, children):

        features_text = [' '.join(x) for x in item['features'].values()]
        features_text = ' '.join(features_text).lower()

        # Figure out type
        ltitle = item['title'].lower()
        # there are item which can be built-in and freestanding
        type_ = "Built-in"
        if 'freestanding' in ltitle or 'freestanding' in features_text:
            type_ = 'Freestanding'
        if 'built-in or can be freestanding' in features_text:
            type_ = 'Built-in or Freestanding'

        text = item['description'] + ' ' + item['blurb']
        ltext = re.sub(r'\s+', ' ', text.lower())

        # Figure out bottle capacity
        bottle_capacity = 'See Spec Sheet'
        energy_star_rated = False
        sabbath_compliance = "No"
        sabbath_mode = False
        bottle_re = r'^Capacity: Wine Bottle \(750 ml\): (\d+)$'
        for val in item['attributes'].values():
            line = u'{0}: {1}'.format(val[0], val[1])
            m = re.match(bottle_re, line)
            if m:
                bottle_capacity = int(m.group(1))
            m = re.match(r'^ENERGY STAR: (.+)$', line)
            if m:
                if m.group(1) == 'na':
                    energy_star_rated = False
                else:
                    energy_star_rated = 'BLYA'
            m = re.match(r'^Sabbath \(Star K Certified\): (Yes|No)$', line)
            if m:
                if m.group(1) == 'Yes':
                    sabbath_compliance = "Yes"
                    sabbath_mode = True
                else:
                    sabbath_compliance = "No"
                    sabbath_mode = True

        # Figure out number of temperature zones
        temperature_zones = 1
        depth = 3
        count_re = r'\d+|%s' % '|'.join(utils.numbers)
        target_re = '(temperature zone)'
        extra_re = r'(\s+[\w-]+){0,%d}' % depth
        pattern_ = r'(?P<n>%s)%s\s+%s'
        pattern = pattern_ % (count_re, extra_re, target_re)
        regex = re.compile(pattern, re.I)

        likely_text = ltext
        greatest = 0
        for card, _, _ in regex.findall(likely_text):
            try:
                card = int(card)
            except ValueError:
                card = utils.numbers.get(card, 0)
            if card > greatest:
                greatest = card
        if greatest:
            temperature_zones = greatest
        if re.match(r'^.+ZWC$', item['upc']):
            temperature_zones = 2
        if 'independently controlled dual-zones' in ltext:
            temperature_zones = 2

        # Figure out configuration
        configuration = "Single"
        if 'column' in ltitle:
            configuration = "Column"
        if re.match(r'^.+WCWC$', item['upc']):
            configuration = "Side-by-side"
        if re.match(r'^.+BVWC$', item['upc']):
            configuration = "Side-by-side"

        # Figure out is_outdoor
        is_outdoor = False
        if 'outdoor' in ltitle:
            is_outdoor = True

        # Figure out is_undercounter
        is_undercounter = False
        if "undercounter" in ltitle or 'undercounter' in features_text:
            is_undercounter = True

        # Figure out is_counter_depth
        is_counter_depth = True
        hinge = 'Both'

        item['attributes'].update({
            'type': ('Type', type_),
            'bottle_capacity': ('Bottle Capacity', bottle_capacity),
            'temperature_zones': ('Temperature Zones', temperature_zones),
            'configuration': ('Configuration', configuration),
            # 'finish': ('Finish', finish),
            'is_outdoor': ('Outdoor', is_outdoor),
            'is_undercounter': ('Undercounter', is_undercounter),
            'is_counter_depth': ('Counter Depth', is_counter_depth),
            'is_energy_rated': ('Energy-Star Rated', energy_star_rated),
            'has_sabbath_mode': ('Sabbath Mode', sabbath_mode),
            'is_sabbath_compliant': ('Sabbath Compliance', sabbath_compliance),
            # 'hinge': ('Hinge', hinge),
        })

    @classmethod
    def handle_ice_maker(cls, item, response, children):
        features_text = [' '.join(x) for x in item['features'].values()]
        features_text = ' '.join(features_text).lower()

        ltitle = item['title'].lower()
        is_outdoor = 'outdoor' in ltitle

        energy_star_rated = False
        sabbath_compliance = "Unknown"
        sabbath_mode = False
        for val in item['attributes'].values():
            line = u'{0}: {1}'.format(val[0], val[1])
            m = re.match(r'^ENERGY STAR: (.+)$', line)
            if m:
                if m.group(1) == 'na':
                    energy_star_rated = False
                else:
                    energy_star_rated = 'BLYA'
            m = re.match(r'^Sabbath \(Star K Certified\): (Yes|No)$', line)
            if m:
                if m.group(1) == 'Yes':
                    sabbath_compliance = "Yes"
                    sabbath_mode = True
                else:
                    sabbath_compliance = "No"
                    sabbath_mode = True

        type_ = "Built-in"
        if 'freestanding' in ltitle or 'freestanding' in features_text:
            type_ = 'Freestanding'
        if 'built-in or can be freestanding' in features_text:
            type_ = 'Built-in or Freestanding'

        item['attributes'].update({
            'type': ('Type', type_),
            'finish': ('Finish', 'Panel-ready'),
            'is_energy_rated': ('Energy-Star Rated', energy_star_rated),
            'has_sabbath_mode': ('Sabbath Mode', sabbath_mode),
            'is_sabbath_compliant': ('Sabbath Compliance', sabbath_compliance),
            'is_outdoor': ('Outdoor', is_outdoor),
        })

    @classmethod
    def handle_refrigeration(cls, item, response, children):
        # Default values
        configuration = 'Column'
        type_ = 'Freestanding'
        function = 'All Refrigerator'
        shape = 'Upright'
        finish = None
        hinge = None
        is_wine_storage = None
        is_outdoor = False
        is_undercounter = False
        is_counter_depth = False
        makes_ice = None

        # Things we will use repeatedly to make determinations.
        ltitle = item['title'].lower()

        if 'glass door refrigerator' in ltitle:
            makes_ice = False
            type_ = 'Freestanding'
            function = 'All Refrigerator'
            shape = 'Upright'

        is_outdoor = False
        if 'outdoor' in ltitle:
            is_outdoor = True
        if re.match(r'^.+OD$', item['upc']):
            is_outdoor = True

        is_undercounter = False

        is_counter_depth = True

        secs_css = 'span[itemprop="description"],.featuresAccordian,.specsTable'
        texts = response.css(secs_css).css('::text').extract()
        text = ' '.join(texts)
        ltext = text.lower()
        ltext = re.sub(r'\s+', ' ', ltext)

        # Ice maker?
        makes_ice = 'ice machine' in ltitle or ' ice ' in ltext

        # Does it mention wine racks, wine storage, or wine shelves?
        n = len(re.findall(r'wine\s+(storage|rack|shel[vf])', ltext))
        is_wine_storage = bool(n)

        # Type and finsih (only the Pro48 series is freestanding)
            # Configuration
        if 'side-by-side' in ltitle:
            configuration = 'Side-by-side'
        elif 'over-under' in ltitle or 'over-and-under' in ltitle:
            configuration = 'Bottom-mount'
        elif 'french door' in ltitle:
            configuration = 'French Door'
        elif 'column' in ltitle:
            configuration = 'Column'
        else:
            configuration = 'Single'

        # Function (aka make up)
        if 'freezer' in ltitle:
            if 'refrigerator' in ltitle:
                function = 'Refrigerator & Freezer'
            else:
                function = 'All Freezer'
        else:
            function = 'All Refrigerator'

        # Shape
        if 'drawer' in ltitle:
            shape = 'Drawer'
        else:
            shape = 'Upright'

        hinge = 'Both'

        temperature_zones = 1
        if 'independently controlled dual-zones' in ltext:
            temperature_zones = 2
        if re.match(r'^.+RR$', item['upc']):
            configuration = 'Side-by-side'
            temperature_zones = 2
        if u'36″' in item['title']:
            configuration = 'Side-by-side'
            temperature_zones = 2

        if 'wine' in item['title']:
            is_wine_storage = True
        item['attributes'].update({
            'type': ('Type', type_),
            'function': ('Function', function),
            'shape': ('Shape', shape),
            'configuration': ('Configuration', configuration),
            'is_outdoor': ('Outdoor', is_outdoor),
            'is_undercounter': ('Undercounter', is_undercounter),
            'is_counter_depth': ('Counter Depth', is_counter_depth),
            'is_wine_storage': ('Stores Wine', is_wine_storage),
            'makes_ice': ('Makes Ice', makes_ice),
            'temperature_zones': ('Temperature Zones', temperature_zones),
            'hinge': ('Hinge', hinge),
        })
