# -*- coding: utf-8 -*-
import scrapy
from sfss.items import ProductItem
import re
from sfss import utils
from scrapy.utils.response import get_base_url
from scrapy.utils.url import urljoin_rfc
import itertools


class ScotsmanhomeiceSpider(scrapy.Spider):
    name = "scotsmanhomeice"
    allowed_domains = ["scotsmanhomeice.com"]
    start_urls = (
        'http://www.scotsmanhomeice.com/',
    )

    def parse(self, response):
        cat_urls = response.css('#nav > ul > li > a::attr(href)').extract()
        for cat_url in cat_urls:
            cat_url = urljoin_rfc(get_base_url(response), cat_url)
            yield scrapy.Request(cat_url, callback=self.parse_category)

    def parse_category(self, response):
        item = ProductItem()
        title_css = '#product-info > h1 *::text'
        item['title'] = re.sub(r'\s+', ' ',' '.join(response.css(title_css).extract()))
        item['url'] = response.url
        item['maker'] = 'Scotsman Ice Systems'

        # Is Basketable?
        item['is_basketable'] = False

        # Is Price Viewable?
        item['is_price_viewable'] = False

        item['categories'] = item['title']


        classes = {
            u'Wine Storage': ('Wine Storage', self.handle_wine_storage),
            u'Brilliance \xae Gourmet Cuber': ('Ice Maker', self.handle_ice_maker),
            u'Brilliance \xae Nugget Ice Machine': ('Ice Maker', self.handle_ice_maker),
            u'Refrigerator': ('Refrigeration', self.handle_refrigeration),
        }
        class_handler = None
        for cla, val in classes.items():
            if cla == item['categories']:
                item['class_'] = val[0]
                class_handler = val[1]
                break

        for model in response.css('.product'):
            m_item = ProductItem(**item)

            blurb_css = '.product-top > .product-copy *::text'
            blurb = model.css(blurb_css).extract()
            m_item['blurb'] = '\n'.join(blurb).strip()

            upc = model.css('.product-details > h4::text').extract()[0]
            upc = re.sub(r'(?i)^Model\-', '', upc)
            m_item['upc'] = upc

            images_css = '.product-image > img::attr(src)'
            m_item['image_urls'] = model.css(images_css).extract()

            m_item['description'] = m_item['blurb']

            m_item['price'] = ''

            features = model.css('.product-details > ul > li')
            features = [' '.join(x.css('::text').extract()) for x in features]
            m_item['features'] = {'Premium features': features}

            features = response.css('.product-extras > .listing_text')
            try:
                features = {x.css('h3::text').extract()[0]: x.css('p::text').extract() for x in features}
                m_item['features'].update(features)
            except:
                pass

            specs = model.css('.product-specs > ul > li')
            specs = ['\n'.join(x.css('::text').extract()) for x in specs]
            for spec in specs:
                spec = spec.split('\n')
                m_item['features'][re.sub(r':$', '', spec[0])] = [spec[1]]

            m_item['file_urls'] = self.process_downloads(response, m_item)

            m_item['attributes'] = {}
            m_item['colors'] = []

            width = self._get_width(m_item, response)
            m_item['attributes']['width'] = 'Width', width

            m_item['attributes'].update({
                'dimensions': ('Dimensions', self._get_dims(m_item, response))
            })

            if class_handler:
                class_handler(m_item, response, None)
            
            yield m_item

    def process_downloads(self, response, item):
        def get_kind(name):
            lname = name.lower()
            kind = 'OTH'
            if 'user guide' in lname:
                kind = 'UAC'
            elif 'owners guide' in lname:
                kind = 'UAC'
            elif 'quick start guide' in lname:
                kind = 'REF'
            elif 'spec' in lname:
                kind = 'REF'
            elif 'warranty' in lname:
                kind = 'REF'
            elif 'programme table' in lname:
                kind = 'REF'
            elif 'quick reference' in lname:
                kind = 'REF'
            elif 'installation' in lname:
                kind = 'INS'
            elif 'design guide' in lname:
                kind = 'DES'
            elif 'owners manual' in lname:
                kind = 'UAC'
            elif '2d' in lname or 'cad' in lname:
                kind = 'C2D'
            elif '3d' in lname:
                kind = 'C3D'
            elif '20' in lname:
                kind = 'CAT'
            return kind
        links = response.css('#product-downloads > ul > li a')
        file_urls = {}
        for link in links:
            url = link.css('::attr(href)').extract()[0]
            url = urljoin_rfc(get_base_url(response), url)
            name = link.css('::text').extract()[0]
            if item['upc'] in name or 'Full Line' in name or re.match(r'^(\w+)\d+$', item['upc']).group(1) in name:
                file_urls[url] = {'name': name, 'kind': get_kind(name)}
        
        return file_urls

    def _get_width(self, item, response):
        for feature in item['features']['Premium features']:
            width_match = re.match(r'^(\d+)\-inch .+$', feature)
            if width_match:
                return width_match.group(1) + u'"'
        return u'"'

    def _get_dims(self, item, response):
        dims = {}
        specs = item['features']
        if 'Dimensions (W x D x H)' in specs:
            dim_text = specs['Dimensions (W x D x H)'][0]
            match = re.match(r'^Unit \(without legs adjusted\): (\d+(\.\d+)?)" x (\d+(\.\d+)?)" x (\d+(\.\d+)?)"$',
                             dim_text)
            if match:
                dims['W'] = match.group(1)
                dims['H'] = match.group(3)
                dims['D'] = match.group(5)
        return u'{W:s}"W x {H:s}"H x {D:s}"D'.format(**dims)

    @classmethod
    def handle_ice_maker(cls, item, response, children):
        ltitle = item['title'].lower()
        ltext = ' '.join(list(itertools.chain(*item['features'].values()))).lower()

        is_outdoor = False
        if 'outdoor' in ltitle or 'outdoor' in ltext:
            is_outdoor = True

        specifications = item['features']

        energy_star_rated = False
        sabbath_compliance = "Unknown"
        sabbath_mode = False
        for v in list(itertools.chain(*item['features'].values())):
            if re.search(r'\bENERGY\s+STAR\b', v):
                energy_star_rated = True
            if re.search(r'\bSabbath\s+mode\b', v):
                sabbath_mode = True
            if re.search(r'\bcomplies with Star-K\b', v):
                sabbath_compliance = 'Yes'
        # if upc ends with *
        if not energy_star_rated and re.match(r'^.+\*$', item['upc']):
            energy_star_rated = True

        type_ = None
        if 'built-in' in ltext:
            type_ = "Built-in"
        if 'freestanding' in ltext:
            type_ = 'Freestanding'
        if not type_:
            type_ = 'Freestanding'

        finish = None
        if 'unfinished front panel' in ltext:
            finish = 'Panel-ready'
        if 'custom wood front panel' in ltext:
            finish = 'Panel-ready'
        if 'stainless' in ltext:
            finish = 'Stainless Steel'
        elif not finish:
            finish = 'Color'
            item['colors'].append({'name': 'Black', 'hexcode': '#000000'})

        item['attributes'].update({
            'type': ('Type', type_),
            'finish': ('Finish', finish),
            'is_energy_rated': ('Energy-Star Rated', energy_star_rated),
            'has_sabbath_mode': ('Sabbath Mode', sabbath_mode),
            'is_sabbath_compliant': ('Sabbath Compliance', sabbath_compliance),
            'is_outdoor': ('Outdoor', is_outdoor),
            'hinge': ('Hinge', 'Both'),
        })

    @classmethod
    def handle_wine_storage(cls, item, response, children):
        # Figure out type
        ltitle = item['title'].lower()
        ltext = ' '.join(list(itertools.chain(*item['features'].values()))).lower()

        type_ = None
        if 'built-in' in ltext:
            type_ = "Built-in"
        if 'freestanding' in ltext:
            type_ = 'Freestanding'
        if not type_:
            type_ = 'Freestanding'

        specifications = item['features']
        # Figure out bottle capacity
        bottle_capacity = 0

        energy_star_rated = False
        sabbath_compliance = "No"
        sabbath_mode = False
        temperature_zones = 1
        for v in list(itertools.chain(*item['features'].values())):
            if re.search(r'\bENERGY\s+STAR\b', v):
                energy_star_rated = True
            if re.search(r'\bSabbath\s+mode\b', v):
                sabbath_mode = True
            if re.search(r'\bcomplies with Star-K\b', v):
                sabbath_compliance = 'Yes'
            match = re.search(r'\b(\d+)-bottle capacity\b', v)
            if match and not bottle_capacity:
                bottle_capacity = int(match.group(1))
            if 'dual zone' in v.lower():
                temperature_zones = 2
            if 'three temperature zones' in v.lower():
                temperature_zones = 3


        # Figure out configuration
        configuration = "Single"
        if 'cellar' in ltitle or 'cellar' in ltext:
            configuration = "Column"
        if 'side-by-side' in ltitle or 'side-by-side' in ltext:
            configuration = "Side-by-side"

        # Figure out is_outdoor
        is_outdoor = False
        if 'outdoor' in ltitle or 'outdoor' in ltext:
            is_outdoor = True

        # Figure out is_undercounter
        is_undercounter = False
        if "undercounter" in ltitle or 'undercounter' in ltext:
            is_undercounter = True

        # Figure out is_counter_depth
        is_counter_depth = True
        
        hinge = None
        if 'reversible glass door' in ltext:
            hinge = 'Both'

        finish = None
        if 'unfinished front panel' in ltext:
            finish = 'Panel-ready'
        if 'custom wood front panel' in ltext:
            finish = 'Panel-ready'
        if 'stainless' in ltext:
            finish = 'Stainless Steel'
        elif not finish:
            finish = 'Color'
            item['colors'].append({'name': 'Black', 'hexcode': '#000000'})

        item['attributes'].update({
            'type': ('Type', type_),
            'bottle_capacity': ('Bottle Capacity', bottle_capacity),
            'temperature_zones': ('Temperature Zones', temperature_zones),
            'configuration': ('Configuration', configuration),
            'finish': ('Finish', finish),
            'is_outdoor': ('Outdoor', is_outdoor),
            'is_undercounter': ('Undercounter', is_undercounter),
            'is_counter_depth': ('Counter Depth', is_counter_depth),
            'is_energy_rated': ('Energy-Star Rated', energy_star_rated),
            'has_sabbath_mode': ('Sabbath Mode', sabbath_mode),
            'is_sabbath_compliant': ('Sabbath Compliance', sabbath_compliance),
            'hinge': ('Hinge', hinge),
        })

    @classmethod
    def handle_refrigeration(cls, item, response, children):
        # Default values
        configuration = 'Column'
        type_ = 'Freestanding'
        function = 'All Refrigerator'
        shape = 'Upright'
        finish = None
        hinge = None
        is_wine_storage = None
        is_outdoor = False
        is_undercounter = False
        is_counter_depth = False
        makes_ice = None

        # Things we will use repeatedly to make determinations.
        ltitle = item['title'].lower()
        ltext = ' '.join(list(itertools.chain(*item['features'].values()))).lower()
        specifications = item['features']

        if 'glass door refrigerator' in ltext:
            makes_ice = False
            type_ = 'Freestanding'
            function = 'All Refrigerator'
            shape = 'Upright'

        is_outdoor = False
        if 'outdoor' in ltitle or 'outdoor' in ltext:
            is_outdoor = True

        is_undercounter = 'undercounter' in ltitle or 'undercounter' in ltext

        is_counter_depth = 'counter depth' in ltitle or 'counter depth' in ltext

        # Ice maker?
        makes_ice = 'ice machine' in ltitle or ' ice ' in ltext

        # Does it mention wine racks, wine storage, or wine shelves?
        n = len(re.findall(r'wine\s+(storage|rack|shel[vf])', ltitle))
        is_wine_storage = bool(n)

        # Type and finsih (only the Pro48 series is freestanding)
            # Configuration
        if 'side-by-side' in ltitle or 'side-by-side' in ltext:
            configuration = 'Side-by-side'
        elif 'side by side' in ltitle or 'side by side' in ltext:
            configuration = 'Side-by-side'
        elif 'over-under' in ltext or 'over-and-under' in ltext:
            configuration = 'Bottom-mount'
        elif 'french door' in ltext:
            configuration = 'French Door'
        elif 'column' in ltext or 'cellar' in ltext:
            configuration = 'Column'
        else:
            configuration = 'Single'

        # Function (aka make up)
        if 'freezer' in ltitle or 'freezer' in ltext:
            if 'refrigerator' in ltitle or 'refrigerator' in ltext:
                function = 'Refrigerator & Freezer'
            else:
                function = 'All Freezer'
        else:
            function = 'All Refrigerator'

        # Shape
        shape = 'Upright'
        if 'drawer' in ltitle.replace('with Drawer', ''):
            shape = 'Drawer'

        hinge = 'Both'

        temperature_zones = 1
        if 'dual zone' in ltitle or 'dual zone' in ltext:
            temperature_zones = 2

        if 'wine' in ltitle:
            is_wine_storage = True

        finish = None
        if 'unfinished front panel' in ltext:
            finish = 'Panel-ready'
        if 'custom wood front panel' in ltext:
            finish = 'Panel-ready'
        if 'stainless' in ltext:
            finish = 'Stainless Steel'
        elif not finish:
            finish = 'Color'
            item['colors'].append({'name': 'Black', 'hexcode': '#000000'})

        item['attributes'].update({
            'type': ('Type', type_),
            'function': ('Function', function),
            'shape': ('Shape', shape),
            'configuration': ('Configuration', configuration),
            'is_outdoor': ('Outdoor', is_outdoor),
            'is_undercounter': ('Undercounter', is_undercounter),
            'is_counter_depth': ('Counter Depth', is_counter_depth),
            'is_wine_storage': ('Stores Wine', is_wine_storage),
            'makes_ice': ('Makes Ice', makes_ice),
            'temperature_zones': ('Temperature Zones', temperature_zones),
            'hinge': ('Hinge', hinge),
            'finish': ('Finish', finish),
        })
