# -*- coding: utf-8 -*-
import scrapy
from sfss.items import ProductItem
import re
from sfss import utils
from scrapy.utils.response import get_base_url
from scrapy.utils.url import urljoin_rfc


class MarvelSpider(scrapy.Spider):
    name = "Marvel"
    allowed_domains = ["agamarvel.com"]
    start_urls = (
        'http://www.agamarvel.com/marvel/products/',
    )

    def parse(self, response):
        cat_urls = response.css('.wpb_wrapper > h4 > a::attr(href)').extract()
        for cat_url in cat_urls:
            yield scrapy.Request(cat_url, callback=self.parse_category)

    def parse_category(self, response):
        prod_urls = response.css('.wpb_wrapper > h6 > a::attr(href)').extract()
        prod_urls = [x for x in prod_urls if 'accessories' not in x and 'accessories' not in x]
        for prod_url in prod_urls:
            yield scrapy.Request(prod_url, callback=self.parse_product)

    def parse_product(self, response):
        item = ProductItem()
        title_css = '.vc_custom_heading > h3::text'
        item['title'] = response.css(title_css).extract()[0]
        item['url'] = response.url
        item['maker'] = 'Marvel'

        # Is Basketable?
        item['is_basketable'] = False

        # Is Price Viewable?
        item['is_price_viewable'] = False
        upc = response.css('.vc_custom_heading > h3::text').extract()[1]
        upc = re.sub(r'(?i)^Model\#\s+', '', upc)
        item['upc'] = re.sub(r'\*+$', '', upc).strip()

        blurb_css = 'meta[property="og:description"]::attr(content)'
        blurb = response.css(blurb_css).extract()
        item['blurb'] = '\n'.join(blurb).strip()

        images_css = '.vc_slide.vc_images_carousel a.prettyphoto::attr(href)'
        item['image_urls'] = response.css(images_css).extract()

        desc_css = 'meta[name="description"]::attr(content)'
        item['description'] = '\n'.join(response.css(desc_css).extract())

        cats = response.css('.breadcrumb-container > ul > li > a')
        cats = [''.join(x.css('*::text').extract()) for x in cats]
        item['categories'] = [u' > '.join(cats[2:-1])]

        classes = {
            u'Beverage Centers': ('Wine Storage', self.handle_wine_storage),
            u'Beverage Center': ('Wine Storage', self.handle_wine_storage),
            u'Wine Cellars': ('Wine Storage', self.handle_wine_storage),
            u'Wine Cellar': ('Wine Storage', self.handle_wine_storage),
            u'Ice Makers': ('Ice Maker', self.handle_ice_maker),
            u'Ice Machine': ('Ice Maker', self.handle_ice_maker),
            u'Refrigerator Freezer': ('Refrigeration',
                                      self.handle_refrigeration),
            u'Refrigerator': ('Refrigeration', self.handle_refrigeration),
            u'Freezer': ('Refrigeration', self.handle_refrigeration),
            u'Refrigerated Drawers': ('Refrigeration',
                                      self.handle_refrigeration),
            u'Refrigeration': ('Refrigeration',
                               self.handle_refrigeration),
        }
        class_handler = None
        for cla, val in classes.items():
            if cla == cats[2:-1][-1]:
                item['class_'] = val[0]
                class_handler = val[1]
                break
        if 'class_' not in item:
            for cla, val in classes.items():
                if cla.lower() in item['title'].lower():
                    item['class_'] = val[0]
                    class_handler = val[1]
                    break
        is_beer_dispenser = '/beer-dispensers/' in response.url
        ft_css = '.wpb_tabs_nav>li>a:contains("Premium Features")::attr(href)'
        features = response.css(ft_css).extract()
        if len(features) > 0:
            features = features[0]
        else:
            ft_css = '.wpb_tabs_nav>li>a:contains("Luxury Class Features")::attr(href)'
            features = response.css(ft_css).extract()[0]

        ft_css = '{0} > div > div > *::text'.format(features)
        features = response.css(ft_css).extract()
        features = [re.sub(ur'^\s*\u2022\s+', '', x).strip() for x in features if x.strip()]
        item['features'] = {'Premium features': features}

        specs_css = '.wpb_tabs_nav>li>a:contains("Specifications")::attr(href)'
        specs = response.css(specs_css).extract()[0]
        specifications = {}
        field = None
        for sp_row in response.css('{0} > div > div > table tr'.format(specs)):
            # if len(sp_row.css('h4')) != len(sp_row.css('strong')):
            #     continue
            tds = sp_row.css('td *::text').extract()
            tds = [re.sub(ur'^\s*\u2022\s+', '', x).strip() for x in tds if x.strip()]
            tds = [re.sub(r'\s+', ' ', x, flags=re.UNICODE) for x in tds if x.strip()]
            if len(tds) == 1 and not field:
                field = tds[0]
                field = re.sub(r'\s+', ' ', field, flags=re.UNICODE).strip()
            elif len(sp_row.css('td>strong::text').extract()) > 0 and len(sp_row.css('td br')) == 0:
                specifications[tds[0]] = tds[1:]
            else:
                values = []
                specifications[field] = []
                for td in sp_row.css('td'):
                    texts = td.css('::text').extract()
                    texts = [re.sub(r'\s+', ' ', x, flags=re.UNICODE) for x in texts if x.strip()]
                    values.append(texts)
                values = [x for x in values if len(x) > 0]
                if len(values) > 0:
                    if len(values) == 1 or len(values[0]) == 1:
                        specifications[field] = [x for x in tds if x.strip()]
                    else:
                        for f, v in zip(values[0], values[1]):
                            f = re.sub(ur'\s+', ' ', f, flags=re.UNICODE).strip()
                            v = re.sub(ur'\s+', ' ', v, flags=re.UNICODE).strip()
                            specifications[field].append(u'{0}: {1}'.format(f, v))
                field = None
        if not specifications:
            field = None
            for row in response.css('{0} .wpb_content_element'.format(specs)):
                texts = row.css('*::text').extract()
                texts = [x for x in texts if x.strip()]
                header_text = row.css('strong::text').extract()
                header_text = [x for x in texts if x.strip()]
                if header_text and not field:
                    field = header_text[0]
                elif texts:
                    specifications[field] = texts
                    field = None
        if None in specifications:
            del specifications[None]
            for td in response.css('{0} > div > div > table tr td'.format(specs)):
                old_texts = []
                for h4 in reversed(td.css('h4')):
                    field = h4.css('::text').extract()[0]
                    texts = h4.xpath('following-sibling::p/text()').extract()
                    cur_texts = texts[:-len(old_texts)] if old_texts else texts
                    specifications[field] = cur_texts
                    old_texts = texts
        item['features'].update(specifications)

        item['attributes'] = {}
        item['colors'] = []

        width = self._get_width(item, response)
        item['attributes']['width'] = 'Width', width

        item['attributes'].update({
            'dimensions': ('Dimensions', self._get_dims(item, response))
        })

        res_css = '.wpb_tabs_nav>li>a:contains("Product Resources")::attr(href)'
        res_id = response.css(res_css).extract()[0]
        res_css = '{0}>div>div>iframe::attr(src)'.format(res_id)
        docs_url = response.css(res_css).extract()[0]

        models = []
        ordd_css = '.wpb_tabs_nav>li>a:contains("Ordering Details")::attr(href)'
        models_id = response.css(ordd_css).extract()[0]
        field = None
        t_css = '{0}>div>div>table'
        tables = response.css(t_css.format(models_id))
        for table in tables:
            try:
                header_text = table.css('h4 > strong::text').extract()[0]
            except:
                header_text = ''
            if 'Accessories' not in header_text:
                for row in table.css('tr')[1:]:
                    tds = row.css('td *::text').extract()
                    tds = [re.sub(ur'(\s+|\xa0)', ' ', x.strip()) for x in tds if x.strip()]
                    if len(tds) == 1 and not field:
                        field = tds[0]
                    elif not field:
                        fields = tds[:len(tds)/2]
                        values = tds[len(tds)/2:]
                        for f, v in zip(fields, values):
                            models.append((f, f, v))
                        field = None
                    else:
                        fields = tds[:len(tds)/2]
                        values = tds[len(tds)/2:]
                        if field:
                            for f, v in zip(fields, values):
                                models.append((field, f, v))
                            field = None
        models = [x for x in models if x[0] != 'Ordering Details' and x[-1] != item['upc']]

        if class_handler:
            class_handler(item, response, None)

        if len(models) > 0 and not is_beer_dispenser:
            upc = item['upc']
            item['upc'] = '(%s)' % upc
            item['children'] = [x[-1] for x in models]

            for model_atr1, model_atr2, model_upc in models:
                child = ProductItem(**item)
                if 'children' in child:
                    del child['children']
                child.update({
                    'canonical_upc': item['upc'],
                    'upc': re.sub(r'\*+$', '', model_upc),
                    'attributes': {},
                    'colors': [],
                    'image_urls': item['image_urls']
                })

                finish = None
                if 'Stainless' in model_atr1 or 'Stainless' in model_atr2:
                    finish = "Stainless Steel"
                if 'Black' in model_atr1 or 'Black' in model_atr1:
                    finish = "Color"
                    child['colors'].append({
                        'name': 'Black',
                        'hexcode': '#000000'})
                if 'White' in model_atr1 or 'White' in model_atr1:
                    finish = "Color"
                    child['colors'].append({
                        'name': 'Black',
                        'hexcode': '#FFFFFF'})
                if 'Integrated' in model_atr1 or 'Integrated' in model_atr2:
                    finish = "Integrated"
                if 'Panel' in model_atr1 or 'Panel' in model_atr1:
                    finish = 'Panel-ready'
                if finish:
                    child['attributes'].update({
                        'finish': ('Finish', finish)})

                hinge = None

                if 'Right Hinge' in model_atr2:
                    hinge = 'Right'
                if 'Left Hinge' in model_atr2:
                    hinge = 'Left'
                if 'Reversible' in model_atr2:
                    hinge = 'Both'
                if hinge:
                    child['attributes'].update({
                        'hinge': ('Hinge', hinge)}
                    )
                request = scrapy.Request(docs_url,
                                         dont_filter=True,
                                         callback=self.parse_downloads)
                request.meta['item'] = child
                yield request

        if not is_beer_dispenser:
            request = scrapy.Request(docs_url,
                                     dont_filter=True,
                                     callback=self.parse_downloads)
            request.meta['item'] = item
            yield request

    def parse_downloads(self, response):
        def get_kind(name):
            lname = name.lower()
            kind = 'OTH'
            if 'user guide' in lname:
                kind = 'UAC'
            elif 'owners guide' in lname:
                kind = 'UAC'
            elif 'quick start guide' in lname:
                kind = 'REF'
            elif 'spec' in lname:
                kind = 'REF'
            elif 'warranty' in lname:
                kind = 'REF'
            elif 'programme table' in lname:
                kind = 'REF'
            elif 'quick reference' in lname:
                kind = 'REF'
            elif 'installation' in lname:
                kind = 'INS'
            elif 'design guide' in lname:
                kind = 'DES'
            elif 'use and care' in lname:
                kind = 'UAC'
            elif '2d' in lname or 'cad' in lname:
                kind = 'C2D'
            elif '3d' in lname:
                kind = 'C3D'
            elif '20' in lname:
                kind = 'CAT'
            return kind
        item = response.meta['item']
        links = response.css('a.doclink')
        file_urls = {}
        for link in links:
            url = link.css('::attr(href)').extract()[0]
            url = urljoin_rfc(get_base_url(response), url)
            name = link.css('::text').extract()[0]
            file_urls[url] = {'name': name, 'kind': get_kind(name)}
        item['file_urls'] = file_urls
        yield item

    def _get_width(self, item, response):
        width_match = re.match(ur'^(\w+\s+)+(\d+)(”|") .+$', item['title'])
        if width_match:
            return width_match.group(2) + u'"'
        width_match = re.match(ur'^(\d+)("|”|\u2033) .+$', item['title'])
        if width_match:
            return width_match.group(1) + u'"'
        return u'"'

    def _get_dims(self, item, response):
        dims = {}
        specs = item['features']
        print specs
        if 'Product Dimensions' in specs:
            dim_text = specs['Product Dimensions'][0]
            match = re.match(ur'^(.+)(“|”)?[^W]*?\bW\b x (.+)(“|”)?[^H]*?\bH\b[^x]* x (.+)(“|”)?[^D]*?\bD\b.*$',
                             dim_text)
            if match:
                dims['W'] = match.group(1)
                dims['H'] = match.group(3)
                dims['D'] = match.group(5)
        else:
            if 'Overall Width' in specs:
                dims['W'] = specs['Overall Width'][0]
            if 'Overall Height from Bottom' in specs:
                dims['H'] = specs['Overall Height from Bottom'][0]
            if 'Overall Height From Bottom' in specs:
                dims['H'] = specs['Overall Height From Bottom'][0]
            if 'Overall Depth From Rear' in specs or 'Overall Depth from Rear' in specs:
                depths = specs['Overall Depth From Rear'] if 'Overall Depth From Rear' in specs else specs['Overall Depth from Rear']
                for v in depths:
                    match = re.match(ur'^To front edge of the handle: (.+)\u2033?$', v)
                    if match:
                        dims['D'] = match.group(1)
                if 'D' not in dims:
                    dims['D'] = re.match(ur'^.+: (.+)(″|\u2033)?$',
                                         specs['Overall Depth From Rear'][0] if 'Overall Depth From Rear' in specs else specs['Overall Depth from Rear'][0]).group(1)
        if 'Specifications' in specs:
            for sp in specs['Specifications']:
                match = re.match(ur'^(Height|Width|Depth).*:(.+)(”|\u2033)$', sp)
                if match and match.group(1)[0] not in dims:
                    dims[match.group(1)[0]] = match.group(2).strip()
        for k, v in dims.items():
            v = re.sub(ur'⅞', ' 7/8', v)
            v = re.sub(ur'¾', ' 3/4', v)
            v = re.sub(ur'⅛', ' 1/8', v)
            v = re.sub(ur'⅝', ' 5/8', v)
            v = re.sub(ur'₃₂', '32', v)
            v = re.sub(ur'½', ' 1/2', v)
            v = re.sub(ur'-', ' ', v)
            v = re.sub(ur'(“|”|\u2033|minimum)', '', v)
            dims[k] = v.strip()
        print dims
        return u'{W:s}"W x {H:s}"H x {D:s}"D'.format(**dims)

    @classmethod
    def handle_wine_storage(cls, item, response, children):
        # Figure out type
        ltitle = item['title'].lower()
        lcategories = ' '.join(item['categories']).lower()
        text = item['description'] + ' ' + item['blurb']
        ltext = re.sub(r'\s+', ' ', text.lower())
        # there are item which can be built-in and freestanding
        type_ = None
        if 'built-in' in lcategories:
            type_ = "Built-in"
        if 'freestanding' in lcategories:
            type_ = 'Freestanding'
        if not type_:
            type_ = 'Freestanding'

        specifications = item['features']
        # Figure out bottle capacity
        bottle_capacity = 0
        if 'Storage Capacity' in specifications:
            for cap in specifications['Storage Capacity']:
                match = re.search(r'\(?(\d+)\)?( wine)? bottles?', cap)
                if match:
                    bottle_capacity = int(match.group(1))

        energy_star_rated = False
        sabbath_compliance = "No"
        sabbath_mode = False
        for f, values in specifications.items():
            for v in values:
                if re.search(r'\bENERGY\s+STAR\b', v):
                    energy_star_rated = True
                if re.search(r'\bSabbath\s+mode\b', v):
                    sabbath_mode = True
                if re.search(r'\bcomplies with Star-K\b', v):
                    sabbath_compliance = 'Yes'
                match = re.search(r'\(?(\d+)\)?( wine)? bottles?', v)
                if match and not bottle_capacity:
                    bottle_capacity = int(match.group(1))
                match = re.search(r'Number of Bottles: (\d+)', v)
                if match and not bottle_capacity:
                    bottle_capacity = int(match.group(1))

        # if upc ends with *
        if not energy_star_rated and re.match(r'^.+\*$', item['upc']):
            energy_star_rated = True

        # Figure out number of temperature zones
        temperature_zones = 1
        if 'dual zone' in ltitle or 'dual zone' in lcategories:
            temperature_zones = 2

        # Figure out configuration
        configuration = "Single"
        if 'cellar' in ltitle or 'cellar' in lcategories:
            configuration = "Column"
        if 'side-by-side' in ltitle or 'side-by-side' in lcategories:
            configuration = "Side-by-side"

        # Figure out is_outdoor
        is_outdoor = False
        if 'outdoor' in ltitle or 'outdoor' in lcategories:
            is_outdoor = True

        # Figure out is_undercounter
        is_undercounter = False
        if "undercounter" in ltitle or 'undercounter' in lcategories:
            is_undercounter = True

        # Figure out is_counter_depth
        is_counter_depth = True
        hinge = 'Both'

        item['attributes'].update({
            'type': ('Type', type_),
            'bottle_capacity': ('Bottle Capacity', bottle_capacity),
            'temperature_zones': ('Temperature Zones', temperature_zones),
            'configuration': ('Configuration', configuration),
            # 'finish': ('Finish', finish),
            'is_outdoor': ('Outdoor', is_outdoor),
            'is_undercounter': ('Undercounter', is_undercounter),
            'is_counter_depth': ('Counter Depth', is_counter_depth),
            'is_energy_rated': ('Energy-Star Rated', energy_star_rated),
            'has_sabbath_mode': ('Sabbath Mode', sabbath_mode),
            'is_sabbath_compliant': ('Sabbath Compliance', sabbath_compliance),
            # 'hinge': ('Hinge', hinge),
        })

    @classmethod
    def handle_ice_maker(cls, item, response, children):
        ltitle = item['title'].lower()
        lcategories = ' '.join(item['categories']).lower()

        is_outdoor = False
        if 'outdoor' in ltitle or 'outdoor' in lcategories:
            is_outdoor = True

        specifications = item['features']

        energy_star_rated = False
        sabbath_compliance = "Unknown"
        sabbath_mode = False
        for f, values in specifications.items():
            for v in values:
                if re.search(r'\bENERGY\s+STAR\b', v):
                    energy_star_rated = True
                if re.search(r'\bSabbath\s+mode\b', v):
                    sabbath_mode = True
                if re.search(r'\bcomplies with Star-K\b', v):
                    sabbath_compliance = 'Yes'
        # if upc ends with *
        if not energy_star_rated and re.match(r'^.+\*$', item['upc']):
            energy_star_rated = True

        type_ = None
        if 'built-in' in lcategories:
            type_ = "Built-in"
        if 'freestanding' in lcategories:
            type_ = 'Freestanding'
        if not type_:
            type_ = 'Freestanding'

        item['attributes'].update({
            'type': ('Type', type_),
            'finish': ('Finish', 'Panel-ready'),
            'is_energy_rated': ('Energy-Star Rated', energy_star_rated),
            'has_sabbath_mode': ('Sabbath Mode', sabbath_mode),
            'is_sabbath_compliant': ('Sabbath Compliance', sabbath_compliance),
            'is_outdoor': ('Outdoor', is_outdoor),
        })

    @classmethod
    def handle_refrigeration(cls, item, response, children):
        # Default values
        configuration = 'Column'
        type_ = 'Freestanding'
        function = 'All Refrigerator'
        shape = 'Upright'
        finish = None
        hinge = None
        is_wine_storage = None
        is_outdoor = False
        is_undercounter = False
        is_counter_depth = False
        makes_ice = None

        # Things we will use repeatedly to make determinations.
        ltitle = item['title'].lower()
        lcategories = ' '.join(item['categories']).lower()
        specifications = item['features']

        if 'glass door refrigerator' in ltitle:
            makes_ice = False
            type_ = 'Freestanding'
            function = 'All Refrigerator'
            shape = 'Upright'

        is_outdoor = False
        if 'outdoor' in ltitle or 'outdoor' in lcategories:
            is_outdoor = True

        is_undercounter = 'undercounter' in ltitle or 'undercounter' in lcategories

        is_counter_depth = 'counter depth' in ltitle or 'counter depth' in lcategories

        # Ice maker?
        makes_ice = 'ice machine' in ltitle or ' ice ' in lcategories

        # Does it mention wine racks, wine storage, or wine shelves?
        n = len(re.findall(r'wine\s+(storage|rack|shel[vf])', ltitle))
        is_wine_storage = bool(n)

        # Type and finsih (only the Pro48 series is freestanding)
            # Configuration
        if 'side-by-side' in ltitle or 'side-by-side' in lcategories:
            configuration = 'Side-by-side'
        elif 'side by side' in ltitle or 'side by side' in lcategories:
            configuration = 'Side-by-side'
        elif 'over-under' in ltitle or 'over-and-under' in ltitle:
            configuration = 'Bottom-mount'
        elif 'french door' in ltitle:
            configuration = 'French Door'
        elif 'column' in ltitle or 'cellar' in ltitle:
            configuration = 'Column'
        else:
            configuration = 'Single'

        # Function (aka make up)
        if 'freezer' in ltitle or 'freezer' in lcategories:
            if 'refrigerator' in ltitle or 'refrigerator' in lcategories:
                function = 'Refrigerator & Freezer'
            else:
                function = 'All Freezer'
        else:
            function = 'All Refrigerator'

        # Shape
        shape = 'Upright'
        if 'drawer' in ltitle.replace('with Drawer', ''):
            shape = 'Drawer'

        hinge = 'Both'

        temperature_zones = 1
        if 'dual zone' in ltitle or 'dual zone' in lcategories:
            temperature_zones = 2

        if 'wine' in ltitle:
            is_wine_storage = True

        item['attributes'].update({
            'type': ('Type', type_),
            'function': ('Function', function),
            'shape': ('Shape', shape),
            'configuration': ('Configuration', configuration),
            'is_outdoor': ('Outdoor', is_outdoor),
            'is_undercounter': ('Undercounter', is_undercounter),
            'is_counter_depth': ('Counter Depth', is_counter_depth),
            'is_wine_storage': ('Stores Wine', is_wine_storage),
            'makes_ice': ('Makes Ice', makes_ice),
            'temperature_zones': ('Temperature Zones', temperature_zones),
            'hinge': ('Hinge', hinge),
        })

# Sorry it's taken longer to get back to you, I've just been really busy. I've gone over the Marvel data and for the most prat it looks alright but there are some problems I need you to fix, I'll include them below. I'll go over Scotsman and get back with you soon.

# List of fixes for Marvel
# - Fix line 30 from causing tracebacks (IndexError)
# - Fix line 178 tracebacks (IndexError)
# - Remove asterisks (*) from all UPCS 
# (Asterisks are usually used as an indicator of some extra text to read at the bottom of the page, as is the case for the UPCs from Marvel's site, they don't really belong)
# - UPC not grabbed for some/at least one product(s). On mine, when I scraped it was: http://www.agamarvel.com/marvel/products/beer-dispensers/marvel-outdoor-24-mobile-single-tap-mo24bs/
# But possibly there are others that dont have their upc grabbed and are eliminated because there is 
# already a product that doesn't have a upc (DuplicateUPCPipeline drops them) so it would probably be
# random every time due to the async nature of Scrapy. (This may be related to line 30 or 178) It is the 
# product that has () as it's upc.

# That's all I've got for the moment, thanks! I'll get back in touch with you soon about Scotsman.

# I forgot about beer dispensers. If you would please, go ahead and scrape Beer Dispensers, even if we don't have class definitions for them, I can make them and put them into vscrape.

# And one last thing I noticed, there's no logic in handle_refrigeration to determine whether the type_ is "Freestanding" or "Built-In" so it just labels everything as "Freestanding". You could probably use the exact same code from wine storage.

# Other than that, Marvel looks real nice!